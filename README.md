# Calendar

The task is to create a demo calendar application using React & Redux. 

You should start by rendering a single month view of a calendar for the current month – along the lines of the below image:

![picture](img/calendar-aug-2018.jpg)

##Features & Requirements:
* Ability to add a new “reminder” (max 30 chars) for a user entered day and time.
* Display reminders on the calendar view in the correct time order.
* Allow the user to select a colour when creating a reminder and display it appropriately.
* Properly handle overflow when multiple reminders appear on the same date.
* Ability to edit reminders – including changing text, day and time & colour.
* Ability to delete reminders.
* Expand the calendar to support more than current month.

Please don't spend more than 2-3 hours on this, please start at the the top of the features and requirements, working your way down getting as many done as possible within the 3 hour timeframe.

## Starting

Please clone this repository, write your code and update this README with a guide of how to run it.

To start you off on the frontend we have created a hello world example, which you can use as the basis for the React Frontend.
To start off, clone the repository and create a new branch.

To install the node modules run:
~~~
npm install
~~~

To create the JS bundle and run the watcher run:
~~~
npm run watch
~~~

You can use whatever node modules you are comfortable with.

## Submission

Either send us a link to the repository on somewhere like github or bitbucket (bitbucket has free private repositories).


